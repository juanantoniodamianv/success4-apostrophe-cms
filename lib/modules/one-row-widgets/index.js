module.exports = {
  extend: 'apostrophe-widgets',
  label: 'One Row',
  contextualOnly: true,
  beforeConstruct: function(self, options) {
    options.addFields = [
      { name: 'icon', type: 'area', contextual: true},
      { name: 'subtitle', type: 'area', contextual: true},
      { name: 'description', type: 'area', contextual: true},
      { name: 'icon2', type: 'area', contextual: true},
      { name: 'subtitle2', type: 'area', contextual: true},
      { name: 'description2', type: 'area', contextual: true}
    ],
    options.arrangeFields || []
  }
};