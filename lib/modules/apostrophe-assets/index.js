// This configures the apostrophe-assets module to push a 'site.less'
// stylesheet by default, and to use jQuery 3.x

module.exports = {
  jQuery: 3,
  stylesheets: [
    {
      name: 'site',
    },
    {
      name: 'bootstrap',
    },
    {
      name: 'style',
    },
    {
      name: 'blog-home'
    }
  ],
  scripts: [
    {
      name: 'site'
    },
    {
      name: 'bootstrap',
    },
    {
      name: 'style',
    },
    {
      name: 'particles',
    },
    {
      name: 'main',
    }
  ]
};
