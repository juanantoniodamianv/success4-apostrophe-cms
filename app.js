var path = require('path');

var apos = require('apostrophe')({
  shortName: 'success4',

  // See lib/modules for basic project-level configuration of our modules
  // responsible for serving static assets, managing page templates and
  // configuring user accounts.
  bundles: [ 'apostrophe-blog' ],

  modules: {

    // Apostrophe module configuration

    // Note: most configuration occurs in the respective
    // modules' directories. See lib/apostrophe-assets/index.js for an example.
    
    // However any modules that are not present by default in Apostrophe must at
    // least have a minimal configuration here: `moduleName: {}`

    // If a template is not found somewhere else, serve it from the top-level
    // `views/` folder of the project

    'apostrophe-templates': { viewsFolderFallback: path.join(__dirname, 'views') },
    'apostrophe-blog': {},
    'apostrophe-blog-pages': {},
    'apostrophe-blog-widgets': {},
    'one-column-widgets': {},
    'two-column-widgets': {},
    'three-column-widgets': {},
    'one-row-widgets': {
      sanitizeHtml: {
        allowedClasses: {
          'div': ['subtitulo', 'description']
        }
      }
    },
    'apostrophe-site': {
      addImageSizes: [
        {
          name: '80x80',
          width: 80,
          height: 80
        }
      ],
    },
    // This configures our default page template
    'apostrophe-pages': {
      sanitizeHtml: {
        allowedClasses: {
          'div': ['subtitulo', 'description']
        }
      },
      filters: {
        // Grab our ancestor pages, with two levels of subpages
        ancestors: {
          children: {
            depth: 2
          }
        },
        // We usually want children of the current page, too
        children: true
      },
      types: [
        {
          name: 'apostrophe-blog-page',
          label: 'Blog'
        },
        {
          name: 'home',
          label: 'Home'
        },
        {
          name: 'default',
          label: 'Default'
        },
        {
          name: 'success',
          label: 'Success'
        }
      ]
    },
    'apostrophe-video-widgets': {}
  }
}); 
